<?php

namespace app\models;
use yii\helpers\ArrayHelper;

use Yii;

/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property integer $leadId
 * @property string $name
 * @property integer $amount
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['leadId', 'name', 'amount'], 'required'],
            [['leadId', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadId' => 'Lead ID',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }
    
    
     /**  public static function getDeals()
    {
    	$allDeals = self::find()->all();
    	$usersFirstname = ArrayHelper::
    		map($allUsers, 'id', 'firstname');
    	$usersLastname = ArrayHelper::
    		map($allUsers, 'id', 'lastname');
    	$users=[];
    	foreach($usersFirstname as $id=>$firstname)
    	{
    		$userd[id]=$firstname. ' ' . $usersLastname[$id];
    	}
    	return $users;
    }*/
    
    
   
    
    
    
}
