<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;


class RbacController extends Controller
{
	public function actionP()
	{
		$auth = Yii::$app->authManager;	
		
		$crudDeals = $auth->createPermission('crudDeals');
		$crudDeals->description = 'Allows to make all cruds';
		$auth->add($crudDeals);
					
		
	}
	
	public function actionC()
	{
		$auth = Yii::$app->authManager;
		
		$teamleader = $auth->getRole('teamleader');
		
		$crudDeals = $auth->getPermission('crudDeals');
		$auth->addChild($teamleader, $crudDeals);	
	}
	
	
	
}